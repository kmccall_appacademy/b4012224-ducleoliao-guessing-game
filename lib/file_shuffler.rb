# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def file_shuffler
  print "Provide a file name:"
  file_name = gets.chomp
  base_name = File.basename(file_name, ".*")
  ext_name = File.extname(file_name)

  new_file = File.open("#{base_name}-shuffled#{ext_name}", "w")
  contents = File.readlines(file_name).shuffle
  new_file.puts contents
  new_file.close
  puts "done!"
end

file_shuffler
#
# def line_shuffler
#   puts "enter the filename to shuffle"
#   input_name = gets.chomp
#   base = File.basename(input_name, ".*")
#   extension = File.extname(input_name)
#
#   x = File.open("#{base}-shuffled#{extension}", "w")
#   x.puts File.readlines(input_name).shuffle
#   x.close
# end
#
# line_shuffler
