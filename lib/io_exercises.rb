# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  computer_num = rand(1..100)
  play(computer_num, [])
end

def play(computer_num, guess_history)
  puts "Guess a number!"
  user_num = gets.chomp.to_i
  puts "User's guess: #{user_num}"
  guess_history << user_num

  if computer_num == user_num
    puts "You won!"
    puts "The number was #{computer_num}!"
    puts "You took #{guess_history.length} to find out!"
  elsif user_num > computer_num
      puts "too high"
      play(computer_num, guess_history)
  else
      puts "too low"
      play(computer_num, guess_history)
  end
end



# def guessing_game
#   true_num = rand(100) + 1
#
#   puts "guess a number"
#   play(true_num, [])
# end
#
# def play(true_num, guesses)
#   guess = gets.chomp.to_i
#   puts "Your guess is #{guess}"
#   guesses << guess
#
#   if guess == true_num
#     puts "Yes, you're correct!"
#     puts "My number was #{true_num}"
#     puts "It took you #{guesses.length} guesses"
#     p "You made the following guesses: #{guesses}"
#
#   elsif guess < true_num
#     puts "too low"
#     play(true_num, guesses)
#   else
#     puts "too high"
#     play(true_num, guesses)
#   end
#
# end
